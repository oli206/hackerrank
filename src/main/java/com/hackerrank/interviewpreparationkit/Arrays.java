package com.hackerrank.interviewpreparationkit;

public class Arrays {

    static int hourglassSum(int[][] arr) {
        int highestHourglass = Integer.MIN_VALUE;
        for (int i = 0; i < 6 - 2; i++) {
            for (int j = 0; j < 6 - 2; j++) {
                int firstLine = arr[i][j] + arr[i][j + 1] + arr[i][j + 2];
                int secondLine = arr[i + 1][j + 1];
                int thirdLine = arr[i + 2][j] + arr[i + 2][j + 1] + arr[i + 2][j + 2];
                int hourGlass = firstLine + secondLine + thirdLine;
                highestHourglass = Math.max(highestHourglass, hourGlass);
            }
        }
        return highestHourglass;
    }

    static int[] rotLeft(int[] a, int d) {
        int[] duplicatedArray = new int[a.length * 2];
        for (int i = 0; i < a.length; i++) {
            duplicatedArray[i] = a[i];
            duplicatedArray[i + a.length] = a[i];
        }
        return java.util.Arrays.copyOfRange(duplicatedArray, d, a.length + d);
    }

    static int minimumBribes(int[] q) {
        int numberOfBribes = 0;

        for (int i = 0; i < q.length; i++) {
            if (q[i] != i + 1) {
                int positionsBribed = q[i] - (i + 1);
                if (positionsBribed > 2) {
                    numberOfBribes = -1;
                    break;
                }
                if (positionsBribed > 0) {
                    numberOfBribes = numberOfBribes + positionsBribed;
                }
            }
        }

        return numberOfBribes;
    }
}
