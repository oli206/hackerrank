package com.hackerrank.interviewpreparationkit;

import java.util.HashMap;
import java.util.Map;

public class DictionariesAndHashMaps {

    static boolean checkMagazine(String[] magazine, String[] note) {
        Map<String,Integer> dictionary = new HashMap<>();
        for (String word : magazine) {
            dictionary.put(word, dictionary.getOrDefault(word, 0) + 1);
        }
        boolean allWordsExist = true;
        for (String word : note) {
            if (dictionary.getOrDefault(word, 0) == 0) {
                allWordsExist = false;
            } else {
                dictionary.put(word, dictionary.get(word) - 1);
            }
        }
        return allWordsExist;
    }

    static String twoStrings(String s1, String s2) {
        Map<String, Integer> firstWordLetters = new HashMap<>();
        for (char letter : s1.toCharArray()) {
            firstWordLetters.put(String.valueOf(letter), 1);
        }

        for (char letter : s2.toCharArray()) {
            if (firstWordLetters.containsKey(String.valueOf(letter))){
                return "YES";
            }
        }
        return "NO";
    }

    static int sherlockAndAnagrams(String s) {
//        Map<String, Integer> letters = new HashMap<>();
//        for (char letter : s.toCharArray()) {
//            letters.put(String.valueOf(letter), letters.getOrDefault(String.valueOf(letter), 0) + 1);
//        }

        return 0;
    }
}
