package com.hackerrank.interviewpreparationkit;

import java.util.Stack;

public class WarmupChallenges {

    static int sockMerchant(int n, int[] ar) {
        System.out.println(" first ");
        int[] socks = new int[100];
        int pairs = 0;
        for (int i = 0; i < n; i++) {
            socks[ar[i] - 1] = socks[ar[i] - 1] + 1;
            if (socks[ar[i] - 1] == 2) {
                pairs++;
                socks[ar[i] - 1] = 0;
            }
        }
        return pairs;
    }

    static int countingValleys(int n, String s) {

        boolean valley = false;
        Stack<Integer> steps = new Stack<>();
        int numberOfValleys = 0;
        for(char c : s.toCharArray()) {

            if (steps.isEmpty() && c == 'U') {
                valley = false;
            } else if (steps.isEmpty() && c == 'D') {
                valley = true;
            }

            if (valley && c == 'U') {
                steps.pop();
            } else if (valley && c == 'D') {
                steps.push(1);
            }

            if (!valley && c == 'U') {
                steps.push(1);
            } else if (!valley && c == 'D') {
                steps.pop();
            }
            if (valley && steps.isEmpty()) {
                numberOfValleys++;
            }
        }

        return numberOfValleys;
    }

    static int jumpingOnClouds(int[] c) {
        return jump(0, c, 0);
    }

    static int jump(int index, int[] c, int jumps) {
        if (index + 1 > c.length - 1) {
            return jumps;
        } if (c[index] == 1) {
            return Integer.MAX_VALUE;
        } else {
            int jumpTwo = jump(index + 2, c, jumps + 1);
            int jumpOne = jump(index + 1, c, jumps + 1);
            return Math.min(jumpOne, jumpTwo);
        }
    }

    static long repeatedString(String s, long n) {
        if (n < s.length()) {
            return countLetters(s.substring(0, (int) n));
        } else {
            int lettersInString = countLetters(s);
            long numberOfRepeatedStrings = n / (long) s.length();
            long remainder = n % (long) s.length();
            return (numberOfRepeatedStrings * lettersInString)
                    + countLetters(s.substring(0, (int) remainder));
        }
    }

    private static int countLetters(String s) {
        int count = 0;
        for(char c : s.toCharArray()) {
            if (c == 'a') {
                count++;
            }
        }
        return count;
    }
}
