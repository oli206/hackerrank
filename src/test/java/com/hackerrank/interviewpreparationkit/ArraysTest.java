package com.hackerrank.interviewpreparationkit;

import org.junit.jupiter.api.Test;

import static com.hackerrank.interviewpreparationkit.Arrays.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArraysTest {

    @Test
    void hourglassSumTest() {
        assertEquals(19, hourglassSum(new int[][]{
                new int[]{1,1,1,0,0,0},
                new int[]{0,1,0,0,0,0},
                new int[]{1,1,1,0,0,0},
                new int[]{0,0,2,4,4,0},
                new int[]{0,0,0,2,0,0},
                new int[]{0,0,1,2,4,0}
        }));
        assertEquals(13, hourglassSum(new int[][]{
                new int[]{1,1,1,0,0,0},
                new int[]{0,1,0,0,0,0},
                new int[]{1,1,1,0,0,0},
                new int[]{0,9,2,-4,-4,0},
                new int[]{0,0,0,-2,0,0},
                new int[]{0,0,-1,-2,-4,0}
        }));
        assertEquals(28, hourglassSum(new int[][]{
                new int[]{-9,-9,-9,1,1,1},
                new int[]{0,-9,0,4,3,2},
                new int[]{-9,-9,-9,1,2,3},
                new int[]{0,0,8,6,6,0},
                new int[]{0,0,0,-2,0,0},
                new int[]{0,0,1,2,4,0}
        }));
        assertEquals(-6, hourglassSum(new int[][]{
                new int[]{-1,-1,0,-9,-2,-2},
                new int[]{-2,-1,-6,-8,-2,-5},
                new int[]{-1,-1,-1,-2,-3,-4},
                new int[]{-1,-9,-2,-4,-4,-5},
                new int[]{-7,-3,-3,-2,-9,-9},
                new int[]{-1,-3,-1,-2,-4,-5}
        }));

    }

    @Test
    void rotLeftTest() {
        assertArrayEquals(new int[]{5,1,2,3,4}, rotLeft(new int[]{1,2,3,4,5}, 4));
        assertArrayEquals(new int[]{77,97,58,1,86,58,26,10,86,51,41,73,89,7,10,1,59,58,84,77}, rotLeft(new int[]{41,73,89,7,10,1,59,58,84,77,77,97,58,1,86,58,26,10,86,51}, 10));
        assertArrayEquals(new int[]{87,97,33,47,70,37,8,53,13,93,71,72,51,100,60}, rotLeft(new int[]{33,47,70,37,8,53,13,93,71,72,51,100,60,87,97}, 13));
    }

    @Test
    void minimumBribesTest() {
        assertEquals(1, minimumBribes(new int[]{1,3,2}));
        assertEquals(3, minimumBribes(new int[]{2,1,5,3,4}));
        assertEquals(-1, minimumBribes(new int[]{2,5,1,3,4}));
        assertEquals(-1, minimumBribes(new int[]{5,1,2,3,7,8,6,4}));
        assertEquals(7, minimumBribes(new int[]{1,2,5,3,7,8,6,4}));
        assertEquals(4, minimumBribes(new int[]{1,2,5,3,4,7,8,6}));
    }
}