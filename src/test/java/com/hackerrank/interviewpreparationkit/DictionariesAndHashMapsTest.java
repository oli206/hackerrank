package com.hackerrank.interviewpreparationkit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.hackerrank.interviewpreparationkit.DictionariesAndHashMaps.*;
import static org.junit.jupiter.api.Assertions.*;

class DictionariesAndHashMapsTest {

    @Test
    void checkMagazineTest() {
        assertTrue(checkMagazine(
                new String[]{"give","me","one","grand","today","night"},
                new String[]{"give","one","grand","today"}
        ));
        assertFalse(checkMagazine(
                new String[]{"two","times","three","is","not","four"},
                new String[]{"two","times","two","is","four"}
        ));
        assertFalse(checkMagazine(
                new String[]{"ive","got","a","lovely","bunch","of","coconuts"},
                new String[]{"ive","got","some","coconuts"}
        ));
    }

    @Test
    void twoStringsTest() {
        assertEquals("YES",twoStrings("hello","world"));
        assertEquals("NO",twoStrings("hi","world"));
        assertEquals("NO",twoStrings("wouldyoulikefries","abcabcabcabcabcabc"));
        assertEquals("YES",twoStrings("hackerrankcommunity","cdecdecdecde"));
        assertEquals("YES",twoStrings("jackandjill","wentupthehill"));
        assertEquals("NO",twoStrings("writetoyourparents","fghmqzldbc"));
        assertEquals("YES",twoStrings("aardvark","apple"));
        assertEquals("NO",twoStrings("beetroot","sandals"));
    }

    @Test
    void sherlockAndAnagramsTest() {
        assertEquals(4, sherlockAndAnagrams("abba"));
        assertEquals(0, sherlockAndAnagrams("abba"));
        assertEquals(3, sherlockAndAnagrams("ifailuhkqq"));
        assertEquals(10, sherlockAndAnagrams("kkkk"));
        assertEquals(5, sherlockAndAnagrams("cdcd"));
    }
}