package com.hackerrank.interviewpreparationkit;

import org.junit.jupiter.api.Test;

import static com.hackerrank.interviewpreparationkit.WarmupChallenges.jumpingOnClouds;
import static com.hackerrank.interviewpreparationkit.WarmupChallenges.repeatedString;
import static org.junit.jupiter.api.Assertions.assertEquals;

class WarmupChallengesTest {

    @Test
    void jumpingOnCloudsTest() {
        assertEquals(4, jumpingOnClouds(new int[]{0, 0, 1, 0, 0, 1, 0}));
        assertEquals(3, jumpingOnClouds(new int[]{0, 0, 0, 1, 0, 0}));
    }

    @Test
    void repeatedStringTest() {
        assertEquals(7, repeatedString("aba", 10));
        assertEquals(1000000000000L, repeatedString("a", 1000000000000L));
    }
}